/*!
 * dailoc
 * 
 * 
 * @author longpt
 * @version 1.0.0
 * Copyright 2019. MIT licensed.
 *///helodist

$('.col-main-content').infiniteScroll({
  // options
  path: '.pagination__next',
  append: '.wp__products',
  history: false,
  hideNav: '.pagination'
});

new WOW().init();